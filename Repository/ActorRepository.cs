﻿using DeltaXAssessmentGeneric.Models;
using DeltaXAssessmentGeneric.ViewModel;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DeltaXAssessmentGeneric.Repository
{
    public class ActorRepository : GenericRepository<Person>, IActorRepository
    {
        private readonly DeltaxassessmentContext _dBContext;

        public ActorRepository(DeltaxassessmentContext context)
            : base(context)
        {
            _dBContext = context;
        }

        public IEnumerable GetAllActors()
        {
            var insertQuery = "execute fetchActor";
            var actorslist = _dBContext.Persons.FromSql(
                   insertQuery
            );

            List<ActorViewModel> actorViewList = new List<ActorViewModel>();

            foreach (var item in actorslist)
            {
                ActorViewModel actorscvm = new ActorViewModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Dob = item.Dob,
                    Sex = item.Sex,
                    Bio = item.Bio,
                    Role = "Actor"
                };
                actorViewList.Add(actorscvm);
            }
            return actorViewList;
        }

        public void InsertActor(List<SqlParameter> updateParameter)
        {
            var insertQuery = "insert into persons (name, dob, bio, role, sex) values (@name, @dob, @bio, @role, @sex)";
            _dBContext.Database.ExecuteSqlCommand(
                   insertQuery, updateParameter
            );
        }

        public void UpdateActor(List<SqlParameter> updateParameter)
        {
            var updateQuery = "update persons set name=@name, dob=@dob, bio=@bio, role=@role, sex=@sex where id = @id";
            _dBContext.Database.ExecuteSqlCommand(
                   updateQuery, updateParameter
            );
        }
    }
}
