﻿using DeltaXAssessmentGeneric.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DeltaXAssessmentGeneric.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly DeltaxassessmentContext _dBContext;
        public GenericRepository(DeltaxassessmentContext context)
        {
            _dBContext = context;
        }

        public void Delete(List<SqlParameter> deleteParameter, string deleteQuery)
        {
            _dBContext.Database.ExecuteSqlCommand(
                deleteQuery, deleteParameter
            );
        }
    }
}