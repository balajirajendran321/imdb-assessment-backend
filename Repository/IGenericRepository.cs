﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace DeltaXAssessmentGeneric.Repository
{
    public interface IGenericRepository<T> where T : class
    {
        /*void GetById(List<SqlParameter> updateParameter, string updateQuery);*/
        void Delete(List<SqlParameter> deleteParameter, string deleteQuery);
    }
}