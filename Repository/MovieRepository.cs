﻿using DeltaXAssessmentGeneric.Models;
using DeltaXAssessmentGeneric.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

namespace DeltaXAssessmentGeneric.Repository
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        private readonly DeltaxassessmentContext _dBContext;

        public MovieRepository(DeltaxassessmentContext context) 
            : base(context)
        {
            _dBContext = context;
        }

        public IEnumerable<MovieViewModel> GetAllMovies()
        {

            var fetchQuery = "execute fetchMovie";
            var moviesList = _dBContext.Movies.FromSql(
                   fetchQuery
            ).ToList();

            List<MovieViewModel> MovieViewList = new List<MovieViewModel>();
            foreach (var movieList in moviesList)
            {
                fetchQuery = $"execute fetchMovieActor @movieid";
                var movieactorsList = _dBContext.MoviesActors.FromSql(fetchQuery,new SqlParameter("@movieid", movieList.Id)).ToList();
                var actor = "";
                foreach (var movieactor in movieactorsList)
                {
                    fetchQuery = $"execute fetchActorById @actorid";
                    var actorList = _dBContext.Persons.FromSql(fetchQuery,new SqlParameter("@actorid", movieactor.Actorsid)).ToArray();
                    actor += actorList[0].Name + ", ";
                }
                MovieViewModel moviecvm = new MovieViewModel
                {
                    Id = movieList.Id,
                    Moviename = movieList.Moviename,
                    Plot = movieList.Plot,
                    Poster = movieList.Poster,
                    Year = movieList.Year,
                    Actors = actor.Substring(0, actor.Length - 2),
                };
                MovieViewList.Add(moviecvm);
            }
            return MovieViewList;
        }

        public MovieViewModel GetByMovieId(int id)
        {
                var fetchQuery = "execute fetchMovieById @movieid";
                var moviesList = _dBContext.Movies.FromSql(
                       fetchQuery, new SqlParameter("@movieid", id)
                ).ToArray();

                fetchQuery = $"execute fetchMovieActor @movieid";
                var movieactorsList = _dBContext.MoviesActors.FromSql(fetchQuery, new SqlParameter("@movieid", moviesList[0].Id)).ToList();
                var actor = "";
                foreach (var movieactor in movieactorsList)
                {
                    fetchQuery = $"execute fetchActorById @actorid";
                    var actorList = _dBContext.Persons.FromSql(fetchQuery, new SqlParameter("@actorid", movieactor.Actorsid)).ToArray();
                    actor += actorList[0].Name + ", ";
                }
                MovieViewModel moviecvm = new MovieViewModel
                {
                    Id = moviesList[0].Id,
                    Moviename = moviesList[0].Moviename,
                    Plot = moviesList[0].Plot,
                    Poster = moviesList[0].Poster,
                    Year = moviesList[0].Year,
                    Actors = actor.Substring(0, actor.Length - 2),
                };
            return moviecvm;
        }

        public void InsertMovie(List<SqlParameter> updateParameter, string actorsId)
        {
            var errorString = "Actors is Required";
            if (actorsId == null)
            {
                throw new ArgumentNullException(errorString);
            }
            var movieInsertQuery = "insert into movies (moviename, plot, poster, year) values(@moviename, @plot, @poster, @year)";
            var movieActorInsertQuery = "insert into movies_actors (movieid, actorsid) values((select TOP 1 id from  movies), @actorid)";
            _dBContext.Database.ExecuteSqlCommand(
                   movieInsertQuery, updateParameter
            );
            foreach (var actorid in actorsId.Split(","))
            {
                List<SqlParameter> actor = new List<SqlParameter>
                {
                    new SqlParameter("@actorid", actorid),
                };
                _dBContext.Database.ExecuteSqlCommand(
                        movieActorInsertQuery, actor
                );
            }
        }

        public void UpdateMovie(List<SqlParameter> updateParameter, string actorsId, int movieId)
        {
            var errorString = "Actors is Required";
            if (actorsId == null)
            {
                throw new ArgumentNullException(errorString);
            }
            var movieInsertQuery = "update movies set moviename = @moviename, plot = @plot, poster = @poster, year = @year where id = @movieid";
            _dBContext.Database.ExecuteSqlCommand(
                   movieInsertQuery, updateParameter
            );
            _dBContext.Database.ExecuteSqlCommand(
                   "delete from movies_actors where movieid = @movieid", updateParameter
            );
            var movieActorInsertQuery = "insert into movies_actors (actorsid, movieid) values(@actorid, @movieid)";
            foreach (var actorid in actorsId.Split(","))
            {
                List<SqlParameter> actor = new List<SqlParameter>
                {
                    new SqlParameter("@actorid", actorid),
                    new SqlParameter("@movieid", movieId),
                };
                _dBContext.Database.ExecuteSqlCommand(
                        movieActorInsertQuery, actor
                );
            }
        }
    }
}
