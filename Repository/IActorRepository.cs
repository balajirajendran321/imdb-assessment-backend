﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using DeltaXAssessmentGeneric.Models;

namespace DeltaXAssessmentGeneric.Repository
{
    public interface IActorRepository : IGenericRepository<Person>
    {
        IEnumerable GetAllActors();
        void InsertActor(List<SqlParameter> insertParameter);
        void UpdateActor(List<SqlParameter> updateParameter);
    }
}