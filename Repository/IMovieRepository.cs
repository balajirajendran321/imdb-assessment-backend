﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DeltaXAssessmentGeneric.Models;
using DeltaXAssessmentGeneric.ViewModel;

namespace DeltaXAssessmentGeneric.Repository
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
        /// <summary>
        ///     It does the crud operation for movies
        /// </summary>
        /// <returns>
        ///     It perform the action based on the method it will return the output
        /// </returns>
        IEnumerable<MovieViewModel> GetAllMovies();
        MovieViewModel GetByMovieId(int id);
        void InsertMovie(List<SqlParameter> updateParameter, string actors);
        void UpdateMovie(List<SqlParameter> updateParameter, string actors, int movieId);
    }
}