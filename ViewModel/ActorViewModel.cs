﻿namespace DeltaXAssessmentGeneric.ViewModel
{
    public class ActorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Dob { get; set; }
        public string Sex { get; set; }
        public string Bio { get; set; }
        public string Role { get; set; }
    }
}
