﻿using System.ComponentModel.DataAnnotations;

namespace DeltaXAssessmentGeneric.ViewModel
{
    public class MovieViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Moviename { get; set; }
        public string Year { get; set; }

        [Required]
        [MaxLength(500)]
        public string Plot { get; set; }

        [Required]
        [Url]
        public string Poster { get; set; }

        [Required]
        public string Actors { get; set; }
    }
}
