﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DeltaXAssessmentGeneric.Models
{
    public class MovieActor
    {
        [JsonIgnore]
        public int Id { get; set; }

        [Required]
        public int Movieid { get; set; }

        [Required]
        public int Actorsid { get; set; }

        public virtual Person Actors { get; set; }
        [JsonIgnore]
        public virtual Movie Movie { get; set; }
    }
}
