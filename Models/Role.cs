﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace DeltaXAssessmentGeneric.Models
{
    public class Role
    {
        public Role()
        {
            Persons = new Collection<Person>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Personrole { get; set; }

        
        public ICollection<Person> Persons { get; }
    }
}
