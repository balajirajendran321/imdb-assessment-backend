﻿using Microsoft.EntityFrameworkCore;
using System;

namespace DeltaXAssessmentGeneric.Models
{
    public  class DeltaxassessmentContext : DbContext
    {
        public DeltaxassessmentContext(DbContextOptions<DeltaxassessmentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Movie> Movies { get; set; }
        public virtual DbSet<MovieActor> MoviesActors { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var errorString = "Model Builder is required";
            if(modelBuilder == null)
            {
                throw new ArgumentNullException(errorString);
            }
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.ToTable("movies");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Moviename)
                    .IsRequired()
                    .HasColumnName("moviename")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Plot)
                    .IsRequired()
                    .HasColumnName("plot")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Poster)
                    .IsRequired()
                    .HasColumnName("poster")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasColumnName("year")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MovieActor>(entity =>
            {
                entity.ToTable("movies_actors");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Actorsid).HasColumnName("actorsid");

                entity.Property(e => e.Movieid).HasColumnName("movieid");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MoviesActors)
                    .HasForeignKey(d => d.Movieid)
                    .HasConstraintName("FK_movies_actors_movies");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.ToTable("persons");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Bio)
                    .IsRequired()
                    .HasColumnName("bio")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Dob)
                    .IsRequired()
                    .HasColumnName("dob")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Role).HasColumnName("role");

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasColumnName("sex")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.RoleNavigation)
                    .WithMany(p => p.Persons)
                    .HasForeignKey(d => d.Role)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_persons_persons");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("roles");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Personrole)
                    .IsRequired()
                    .HasColumnName("personrole")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }

    }
}
