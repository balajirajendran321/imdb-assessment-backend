﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace DeltaXAssessmentGeneric.Models
{
    public class Movie
    {
        public Movie()
        {
            MoviesActors = new Collection<MovieActor>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Moviename { get; set; }

        [Required]
        public string Year { get; set; }

        [Required]
        [MaxLength(500)]
        public string Plot { get; set; }

        [Required]
        [Url]
        public string Poster { get; set; }
        public int Status { get; set; }

        public ICollection<MovieActor> MoviesActors { get; }

    }
}
