﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeltaXAssessmentGeneric.Models
{
    public  class Person
    {
        
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        public string Dob { get; set; }

        [Required]
        public string Sex { get; set; }

        [Required]
        [MaxLength(500)]
        public string Bio { get; set; }

        [ForeignKey("Role")]
        [Required]
        public int Role { get; set; }
        public Role RoleNavigation { get; set; }

        public ICollection<MovieActor> MoviesActors { get; set; }

    }
}
