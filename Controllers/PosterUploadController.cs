﻿using System;
using System.Threading.Tasks;
using Firebase.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DeltaXAssessmentGeneric.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PosterUploadController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public PosterUploadController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Index(IFormFile poster)
        {
            if(poster == null)
            {
                return StatusCode(400);
            }

            var posterName = await new FirebaseStorage(_configuration.GetConnectionString("FirebaseConnection"))
                .Child("Image1")
                .Child(Guid.NewGuid().ToString() + ".png")
                .PutAsync(poster.OpenReadStream());

            return Ok(posterName);
        }
    }
}
