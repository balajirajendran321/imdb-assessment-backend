﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using DeltaXAssessmentGeneric.Models;
using DeltaXAssessmentGeneric.Repository;
using Microsoft.AspNetCore.Mvc;


namespace DeltaXAssessmentGeneric.Controllers
{
    [Route("api/actor")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly IActorRepository actorRepository;

        public ActorsController(IActorRepository actorRepository)
        {
            this.actorRepository = actorRepository;
        }

        // GET: api/actor
        [HttpGet]
        public IEnumerable Get()
        {
            return actorRepository.GetAllActors();
        }

        // POST: api/actor
        [HttpPost]
        public IActionResult Add(Person person)
        {
            var errorString = "Actor is Required";
            if (person == null)
            {
                throw new ArgumentNullException(errorString);
            }
            List<SqlParameter> insertParameter = new List<SqlParameter>
                {
                    new SqlParameter("@name", person.Name),
                    new SqlParameter("@dob", person.Dob),
                    new SqlParameter("@bio", person.Bio),
                    new SqlParameter("@role", person.Role),
                    new SqlParameter("@sex", person.Sex)
                };
            actorRepository.InsertActor(insertParameter);
            return Ok("Actor created Successfully");
        }

        // PUT: api/actor
        [HttpPut]
        public IActionResult Put(Person person)
        {
            var errorString = "Actor is Required";
            if (person == null)
            {
                throw new ArgumentNullException(errorString);
            }
            List<SqlParameter> updateParameter = new List<SqlParameter>
                {
                  new SqlParameter("@id", person.Id),
                    new SqlParameter("@name", person.Name),
                    new SqlParameter("@dob", person.Dob),
                    new SqlParameter("@bio", person.Bio),
                    new SqlParameter("@role", person.Role),
                    new SqlParameter("@sex", person.Sex)
                };
            actorRepository.UpdateActor(updateParameter);
            return Ok("Actor Updated Successfully");
        }

        // DELETE: api/actor/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            List<SqlParameter> deleteParameter = new List<SqlParameter>
                {
                   new SqlParameter("@id", id),
                };
            actorRepository.Delete(deleteParameter, "delete persons where id = @id");
            return Ok("Actor Deleted Successfully");
        }
    }
}
