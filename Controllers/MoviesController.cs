﻿using System.Collections;
using DeltaXAssessmentGeneric.Repository;
using Microsoft.AspNetCore.Mvc;
using DeltaXAssessmentGeneric.ViewModel;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;

namespace DeltaXAssessmentGeneric.Controllers
{
    [Route("api/movie")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository movieRepository;

        public MoviesController(IMovieRepository movieRepository)
        {
            this.movieRepository = movieRepository;
        }

        // GET: api/movie
        [HttpGet]
        public IEnumerable Get()
        {
            return movieRepository.GetAllMovies();
        }

        // GET: api/movie/1
        [HttpGet("{id}")]
        public MovieViewModel Get(int id)
        {
            return movieRepository.GetByMovieId(id);
        }

        // POST: api/movie
        [HttpPost]
        public IActionResult Add(MovieViewModel movie)
        {
            var errorString = "Movie is Required";
            if (movie == null)
            {
                throw new ArgumentNullException(errorString);
            }
            List<SqlParameter> insertParameter = new List<SqlParameter>
                {
                    new SqlParameter("@moviename", movie.Moviename),
                    new SqlParameter("@plot", movie.Plot),
                    new SqlParameter("@poster", movie.Poster),
                    new SqlParameter("@year", movie.Year),
                };
            movieRepository.InsertMovie(insertParameter, movie.Actors);
            return Ok("Movie Created Successully");
        }

        // PUT: api/movie
        [HttpPut]
        public IActionResult Put(MovieViewModel movie)
        {
            var errorString = "Movie is Required";
            if (movie == null)
            {
                throw new ArgumentNullException(errorString);
            }
            List<SqlParameter> updateParameter = new List<SqlParameter>
                {
                    new SqlParameter("@movieid", movie.Id),
                    new SqlParameter("@moviename", movie.Moviename),
                    new SqlParameter("@plot", movie.Plot),
                    new SqlParameter("@poster", movie.Poster),
                    new SqlParameter("@year", movie.Year),
                };
            movieRepository.UpdateMovie(updateParameter, movie.Actors,movie.Id);
            return Ok("Movie Updated Successully");
        }

        // DELETE: api/movie/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            List<SqlParameter> deleteParameter = new List<SqlParameter>
                {
                   new SqlParameter("@id", id),
                };
            movieRepository.Delete(deleteParameter, "delete movies where id = @id");
            return Ok("Movie Deleted Successully");
        }
    }
}
